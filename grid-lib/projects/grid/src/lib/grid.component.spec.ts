import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridComponent } from './grid.component';

describe('GridComponent', () => {
  let component: GridComponent;
  let fixture: ComponentFixture<GridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test pageChange movepage() be called', ()=>{
    spyOn(component.pageChange, 'emit');
    component.movePage("next");
    expect(component.pageChange.emit).toHaveBeenCalled();
    expect(component.pageChange.emit).toHaveBeenCalledWith("next")
    component.movePage("previous");
    expect(component.pageChange.emit).toHaveBeenCalled();
    expect(component.pageChange.emit).toHaveBeenCalledWith("previous")
  });

  it('test pageChange movepage() not be called', ()=>{
    spyOn(component.pageChange, 'emit');
    component.movePage("test");
    expect(component.pageChange.emit).not.toHaveBeenCalled();
  });

});
