import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'lib-grid',
  template: `
  <div #table>
    <ng-content></ng-content>
  </div>
  `,
  styles: [
  ]
})
export class GridComponent implements OnInit {

  private _model: any[];
  @Input()
  set model(model: any[]) {
    this._model = model;
  }
  get model() {
    return this._model;
  }

  @Output() pageChange: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    console.log('start of grid component');
  }

  movePage(direction: string) {
    console.log(direction);
    if (direction === 'next' ||
      direction === 'previous') {
      this.pageChange.emit(direction);
    }
  }


}
