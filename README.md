
* features
1. grid-lib is the angular library, will be used in test-app
2. data transmit between parent component child component has been implemented by using input and output decorator
3. service has been used
4. decorator viewchild has been used
5. function key listener has been used
6. tests have been implemented in both projects
7. those projects are developed in angular 11 

* how to run
1. go grid-lib folder, run npm install, then npm run package
2. go to test-app folder, run npm install ../grid-lib/dist/grid/grid-0.0.1.tgz
3. after npm install local package done, run npm install
4. run npm start, then open browser with localhost:4200
5. run tests with npm run test


