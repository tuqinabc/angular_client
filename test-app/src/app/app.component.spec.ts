import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { GridComponent, GridModule } from 'grid';
import { TableModule } from 'primeng/table';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TableModule,
        GridModule,
        RouterTestingModule
      ],
      declarations: [
        GridComponent,
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'test-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('test-app');
  });

  it('should call handleKeyboardEvent with arrowup', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.dataGrid = TestBed.createComponent(GridComponent).componentInstance as GridComponent;
    fixture.detectChanges();
    spyOn(app.dataGrid, 'movePage');
    
    let upPress = new KeyboardEvent('keydown', {key: 'ArrowUp'});
    app.handleKeyboardEvent(upPress);
    expect(app.dataGrid.movePage).toHaveBeenCalledOnceWith("previous");
  });


  it('should call handleKeyboardEvent with arrowdown', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.dataGrid = TestBed.createComponent(GridComponent).componentInstance as GridComponent;
    fixture.detectChanges();
    spyOn(app.dataGrid, 'movePage');
    
    let downPress = new KeyboardEvent('keydown', {key: 'ArrowDown'});
    app.handleKeyboardEvent(downPress);
    expect(app.dataGrid.movePage).toHaveBeenCalledOnceWith("next");
  });


});
