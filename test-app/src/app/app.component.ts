import { Component, ViewChild, OnInit, HostListener } from '@angular/core';
import { ProductService } from './productservice';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'test-app';
  cols: any[];
  public formData: any;

  @ViewChild('dataGrid') dataGrid: any;

  pageChange(dirction) {
    console.log(dirction + '--------------->');
  }
  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.getProductsSmall().then(data => this.formData = data);
    this.cols = [
      { field: 'code', header: 'Code' },
      { field: 'name', header: 'Name' },
      { field: 'category', header: 'Category' },
      { field: 'quantity', header: 'Quantity' }
    ]
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    event.preventDefault();
    event.stopPropagation();
    if (event.key == 'ArrowUp') {
      this.dataGrid.movePage('previous');
    } else if (event.key == 'ArrowDown') {
      this.dataGrid.movePage('next');
    } else {
      console.log("not supported now!!!");
    }
  }
}
